/* Mini-Activity
Initialize npm in discussion folder
Install express, mongoose and nodemon modules
Start the nodemon under scripts in package.json file
Create a .gitignore file and add the node_modules

In your index.js- create simple express server 

ENDS in 15 MINUTES- 6:01PM
*/

// Express server

const express = require('express'); //express module
const { Mongoose } = require('mongoose');

const port = 4000; // any port as long as free
const app = express(); //create server thru variable 'app' thru module express.

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// feedback when the port is running
app.listen(port, () => console.log(`Server is running at port ${port}`))

//Connecting MongoDB
/* Connect to the database by passing in your connection string, remember to replace the password and database names with actual values

Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error

By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application

{ newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB

Syntax
	mongoose.connect("<MongoDB Atlas connection string>", { useNewUrlParser : true });

Get the connection string from your MongoDB Atlas, under Deployment > Database > click connect > 2nd option > copy SRV string

Make sure to change the password and your desired database name */

const mongoose = require('mongoose'); // For connecting MongoDB

mongoose.connect("mongodb+srv://iamxdv:zuittb145@cluster0.bibta.mongodb.net/session31?retryWrites=true&w=majority", 
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
);

// Tho this is not required but for the status clarity 
let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));

// Mongoose Schemas
	// schema ===  blueprint of your data

    const taskSchema = new mongoose.Schema({

        name: String,
        // status: {
        //     type: String,
        //     default: 'pending'
        // },

        email: String,
        username : String,
        password : String,
        age : Number,
        isAdmin : false
    });
    
    
    // Model
    const Task = mongoose.model("Task", taskSchema);
    
    // Business Logic
    
    // Creating a new task
    /* app.post("/tasks", (req, res) => {
    
        Task.findOne({name : req.body.name}, (err, result) => {
    
            if(result != null && result.name === req.body.name){
    
                return res.send(`Duplicate task found: ${err}`)
    
            } else {
    
                let newTask = new Task({
                    name : req.body.name
                });
    
                newTask.save((saveErr, savedTask) => {
                    if(saveErr){
                        return console.error(saveErr)
                    } else {
                        return res.status(200).send(`New task created : ${savedTask}`)
                    };
                });
            };
        });
    });
    
    // Retrieving all tasks
    app.get("/tasks", (req, res) => {
    
        Task.find({}, (err, result) => {
    
            if(err){
                
                return console.log(err);
            
            } else {
    
                return res.status(200).json({
                    tasks : result
                })
            }
        })
    })
    
    // Updating Task Name
    app.put("/tasks/update/:taskId", (req, res) => {
    
        let taskId = req.params.taskId
        let name = req.body.name
    
        Task.findByIdAndUpdate(taskId, {name: name}, (err, updatedTask) => {
            if(err){
                console.log(err)
            } else {
                res.send(`Congratulations the task has been updated`);
            }
        })
    })
    
    // Delete Task
    app.delete("/tasks/archive-task/:taskId", (req, res) => {
    
        let taskId = req.params.taskId;
    
        Task.findByIdAndDelete(taskId, (err, deletedTask) => {
            if(err){
                console.log(err)
            } else {
                res.send(`${deletedTask} has been deleted`)
            }
        })
    })

*/

/* 
1. Create a User schema.
	<email>: String,	
	<username> : String,
	<password> : String,
	<age> : Number
	<isAdmin> : Boolean, default: false
    
2. Create a User model.

3. Create a POST route to register a user.

4. Name the endpoint /users/signup
	Business Logic:
		Add a functionality to check if there are duplicate users
		- If the email already exists in the database, we return an error
		- If the email doesn't exist in the database, we add the in the database
		The user data will be coming from the request's body
		Create a new User object with a “email, "username", "password" and “age” fields/properties

5. Process a POST request at the /users/signup route using postman to register a user.

6. Retrieve all users that registered using the endpoint /users.

7. Process a GET request at the /users in postman to retrieve all users.

8. Create an endpoint that will be able to update the user’s username. Name the endpoint /users/update-user/:wildcard

9. Process a PUT request in postman to update a user.

10. Create an endpoint that can delete a user. Name the endpoint /users/archive-user/:wildcard

11. Process a DELETE request in postman to delete a user.

12. Create an image folder containing all your postman outputs.

13. Export your postman collection
*/


//Business Logic
    
    // Creating a new task
    app.post("/users/signup", (req, res) => {
    
        Task.findOne({email : req.body.email}, (err, result) => {
    
            if(result != null && result.email === req.body.email){
    
                return res.send(`Duplicate task found: ${err}`)
    
            } else {
    
                let newTask = new Task({
                    email : req.body.email,
                    username : req.body.username,
                    password : req.body.password,
                    age : req.body.age
                });
    
                newTask.save((saveErr, savedTask) => {
                    if(saveErr){
                        return console.error(saveErr)
                    } else {
                        return res.status(200).send(`New task created : ${savedTask}`)
                    };
                });
            };
        });
    });

// Retrieving all tasks
app.get("/users", (req, res) => {
    
    Task.find({}, (err, result) => {

        if(err){
            
            return console.log(err);
        
        } else {

            return res.status(200).json({
                tasks : result
            })
        }
    })
})

// Updating Task
app.put("/users/update-user/:wildcard", (req, res) => {

    let taskId = req.params.taskId
    let email = req.body.email

    Task.findByIdAndUpdate(taskId, {email: email}, (err, updatedTask) => {
        if(err){
            console.log(err)
        } else {
            res.send(`Congratulations the task has been updated`);
        }
    })
})

// Delete Task
app.delete("/users/archive-user/:wildcard", (req, res) => {

    let taskId = req.params.taskId;

    Task.findByIdAndDelete(taskId, (err, deletedTask) => {
        if(err){
            console.log(err)
        } else {
            res.send(`${deletedTask} has been deleted`)
        }
    })
})